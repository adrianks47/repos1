-- Ex 1 :
isEven :: Int -> Bool
isEven n = mod n 2 == 0

-- Mod = Resto da divis�o

-- Ex 2 :
somaquad :: Int -> Int -> Int
somaquad x y = (x^2)+(y^2)

-- Ex 3 :
doubleFirst :: [Int] -> Int
doubleFirst x = head x^2

-- Ex4 :
hasEqHeads :: [Int] -> [Int] -> Bool
hasEqHeads x y = (head x)==(head y)

-- Ex5 :
addMr :: [String] -> [String]
addMr x = map ("Mr. "++)(x)

--Ex 6 :
spaceN :: String -> Int
spaceN x = length( filter(==' ')(x) )

--Ex 7:
mapAux :: Double -> Double
mapAux y = 3*y^2 + 2/y + 1
mapFunc :: [Double] -> [Double]
mapFunc x= map(mapAux) x

--Ex 8:
idade :: [Integer] -> [Integer]
idade x = map(2015-)( filter(>1970)( map(2015-) x ) )

-- Ex 9:
serie :: Double -> [Double] -> Double
serie m n = sum( map ( /m )n )

-- Ex 10:
charFound :: Char -> String -> Bool
charFound c s = length ( filter( == c) s ) > 0

-- Ex 11
htmlListItems :: [String] -> [String]
htmlListItems x = map( ++"</LI>")( map("<LI>"++)x )

-- Ex 12
-- takeWhile ( >= 0)[0,5,7,8,-2,5,6,-3,7,8]

-- Ex 13
endInA :: String -> Bool
endInA x = (last x) == 'a'

female :: [String] -> [String]
female x = filter( endInA ) x